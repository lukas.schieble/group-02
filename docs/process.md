# Process

Here, we will describe our process from the formation of the groups until the final presentation in January. 
## Group formation (24/10/23)  

You can watch our individual docs (Module 5; [Ana](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/anapatricia.fernandes), [Diane](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/diane.bilgischer), [Lukas](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/lukas.schieble) and [Jean](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/jean.boussier)) to get more detailed information on our individual thoughts. Here we will only focus on the group's perspective.  

|Name|Object|Problematic|  
|--------------------|:--------------------:|--------------------|  
|Ana|Empty Pencil Container|Plastic Waste, consumerism|  
|Jean|Adjustabme Wrench|Building railways on your own (Cherchez pas, c'est Jean)|  
|Lukas|1€ Coin|Money-dominated society|  
|Diane|Architecture Book|Lack of housing|  


So, everyone had to bring an object to class and go talk to other people that brought similar objects or just an object representing an interesting problematic. Pretty naturally, little groups of 3-4 people were forming, or at least, that's the theory...  
For our group, things happened quite differently. Lukas talked with 
## Problem brainstorming and Group Dynamics (6-7/11/23)

### Group Dynamics

### Brainstorming

## Prototyping a Solution and Hackathon (13-14/11/23)

## Try, Fail, Repeat (our weekly Meetings)

### 21/11/23

### 27/11/23

## Pre-Jury

