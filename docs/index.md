# Group and project presentation

## About our team

Our team is composed of :

- [Ana Fernandes](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/anapatricia.fernandes), 1st year of Master in Biomedical Engineering and currently doing an Erasmus exchacnge in Brussels   
- [Jean Boussier](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/jean.boussier), 3rd year of Bachelor in Bioengineering at ULB   
- [Diane Bilgischer](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/diane.bilgischer), 3rd year of Bachelor in Bioengineering at ULB    
- [Lukas Schieble](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/lukas.schieble), 3rd year of Bachelor in Bioengineering at ULB   

Here we are :

![]()

As you can see, 3 of us are studying Bioengineering

## Our Project abstract

Describe here a small summary of your project.

![Graphical abstract](images/firstname.surname-slide.png)


